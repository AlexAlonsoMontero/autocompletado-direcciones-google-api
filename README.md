# Autocompletado direcciones Google Api

Codigo para autocompletado de direcciones de Google maps tanto con Vanilla JavaScript, como con React

**ES NECESARIO DARSE DE ALTA EN LA API DE GOOGLE AUTOCOMPLETE PARA PODER DISFRUTAR ESTA FUNCIONALIDAD**

<h1>React js</h1>
- npm install react-google-places-autocomplete
- En la carpeta React comparto un fichero con el código para crear un coponente que creara un desplegable.
- Esta configurado con restricción a España

<h1>JavaScript (JQuery)</h1>
-Es necesario utilizar JQuery

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
```


-En el html hay que importar el siguiente script

```html
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=TU API DE GOOGLE"></script>
```
-En la carpeta JavaScript encontrarás un index html de jemplo  y el mainjs
-Esta configurado sin restricción de paises

